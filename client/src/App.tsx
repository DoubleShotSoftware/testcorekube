import React, {Dispatch, useState} from 'react';
import './App.css';

const axios = require('axios');
function getRandomString(setRandomString: Dispatch<string | undefined>) {
    axios.get('/api/random/v1/string')
        .then(function (response: any) {
            // handle success
            console.log(response);
            setRandomString(response.data as string)
        })
        .catch(function (error: any) {
            // handle error
            console.log(error);
        })
        .finally(function () {
            // always executed
        });

}

function getRandomNumber(setRandomNumber: Dispatch<number | undefined>) {
    axios.get('/api/random/v1/number')
        .then(function (response: any) {
            // handle success
            console.log(response);
            setRandomNumber(response.data as number)
        })
        .catch(function (error: any) {
            // handle error
            console.log(error);
        })
        .finally(function () {
            // always executed
        });

}

function App() {
    const [randomString, setRandomString] = useState<string | undefined>(undefined)
    const [randomNumber, setRandomNumber] = useState<number | undefined>(undefined)
    return (
        <div className="App">
            <table>
                <thead>
                <tr>
                    <td>Action</td>
                    <td>Result</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <button onClick={e => getRandomString(setRandomString)}>
                            Get Random String
                        </button>
                    </td>
                    <td>{randomString}</td>
                </tr>
                <tr>
                    <td>
                        <button onClick={e => getRandomNumber(setRandomNumber)}>
                            Get Random Number
                        </button>
                    </td>
                    <td>{randomNumber}</td>
                </tr>
                </tbody>
            </table>
        </div>
    );
}

export default App;
