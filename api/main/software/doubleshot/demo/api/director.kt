package software.doubleshot.demo.api

import io.micrometer.core.annotation.Timed

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication
@Timed
@RestController
class RandomNumber {
    private val charPool: List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')

    @GetMapping("/api/random")
    fun hello() = "Hello"

    @GetMapping("/api/random/v1/string")
    fun randomString() = (1..256)
            .map { _ -> kotlin.random.Random.nextInt(0, charPool.size) }
            .map(charPool::get)
            .joinToString("")

    @GetMapping("/api/random/v1/number")
    fun randomNumber(): Int {
        val start = 0
        val end = 100
        require(start <= end) { "Illegal Argument" }
        return (start..end).random()
    }
}

fun main(args: Array<String>) {
    runApplication<RandomNumber>(*args)
}
