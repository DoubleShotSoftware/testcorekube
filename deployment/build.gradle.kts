plugins {
    kotlin("jvm") version "1.3.70"
    application
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()
}
kotlin {
    sourceSets {
        main {
            kotlin.setSrcDirs(
                    mutableListOf("main", "generated")
            )
            resources.srcDir("main/resources")
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation("io.fabric8:kubernetes-client:4.10.2")
                implementation("com.fkorotkov:kubernetes-dsl:2.8")
            }
        }
    }
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}

application {
    mainClassName = "software.doubleshot.demo.deployment.DirectorKt"
}
