package software.doubleshot.demo.deployment

import com.fkorotkov.kubernetes.apps.metadata
import com.fkorotkov.kubernetes.apps.selector
import com.fkorotkov.kubernetes.apps.spec
import com.fkorotkov.kubernetes.apps.template
import com.fkorotkov.kubernetes.metadata
import com.fkorotkov.kubernetes.newContainer
import com.fkorotkov.kubernetes.newContainerPort
import com.fkorotkov.kubernetes.spec
import io.fabric8.kubernetes.api.model.apps.Deployment


class DemoAPIDeployment : Deployment() {
    private val deploymentName = "$applicationName-api-deployment"

    init {
        metadata {
            name = deploymentName
            annotations = gitLabAnnotations + ingressAnnotations
            labels = applicationLabels
        }
        spec {
            replicas = 3
            selector {
                matchLabels = applicationLabels
            }
            template {
                metadata {
                    labels = applicationLabels
                    annotations = gitLabAnnotations
                }
                spec {
                    containers = listOf(
                            newContainer {
                                name = deploymentName
                                image = applicationImage
                                imagePullPolicy = "Always"
                                ports = listOf(
                                        newContainerPort {
                                            containerPort = DemoAPIService.apiPort
                                        }
                                )

                            }
                    )
                }
            }
        }
    }
}