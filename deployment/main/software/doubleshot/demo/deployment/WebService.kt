package software.doubleshot.demo.deployment

import com.fkorotkov.kubernetes.metadata
import com.fkorotkov.kubernetes.newServicePort
import com.fkorotkov.kubernetes.spec
import io.fabric8.kubernetes.api.model.IntOrString
import io.fabric8.kubernetes.api.model.Service


class WebService : Service() {
    companion object {
        const val webPort = 80
        const val serviceName = "${applicationName}-web-svc"
    }

    init {
        metadata {
            name = serviceName
            labels = applicationLabels
            annotations = gitLabAnnotations + ingressAnnotations
        }
        spec {
            selector = applicationLabels
            ports = listOf(
                    newServicePort {
                        port = webPort
                        targetPort = IntOrString(webPort)
                    }
            )
            clusterIP = "None"
        }
    }
}
