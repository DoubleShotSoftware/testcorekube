package software.doubleshot.demo.deployment

import com.fkorotkov.kubernetes.metadata
import com.fkorotkov.kubernetes.newServicePort
import com.fkorotkov.kubernetes.spec
import io.fabric8.kubernetes.api.model.IntOrString
import io.fabric8.kubernetes.api.model.Service


class DemoAPIService : Service() {
    companion object {
        const val apiPort = 8080
        const val serviceName = "${applicationName}-api-svc"
    }

    init {
        metadata {
            name = serviceName
            labels = applicationLabels
            annotations = gitLabAnnotations + ingressAnnotations
        }
        spec {
            selector = applicationLabels
            ports = listOf(
                    newServicePort {
                        port = apiPort
                        targetPort = IntOrString(apiPort)
                    }
            )
            clusterIP = "None"
        }
    }
}
