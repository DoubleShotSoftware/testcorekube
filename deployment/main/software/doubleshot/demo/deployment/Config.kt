
// GitLab Specific Elements
object EnvConfig {
    val ciProjectPathSlug = System.getenv("CI_PROJECT_PATH_SLUG") ?: ""
    val ciEnvironmentSlug = System.getenv("CI_ENVIRONMENT_SLUG") ?: ""
    val dbPassword = System.getenv("dbPassword") ?: "ghost"
    val dbUser = System.getenv("dbUser") ?: "ghost"
    val database = System.getenv("database") ?: "ghost"
    val commitSha = System.getenv("CI_COMMIT_SHA") ?: ""
    val envUrl = System.getenv("CI_ENVIRONMENT_URL") ?: "https://blog.animus.design"
    val hostName = envUrl
            .replace("https://", "")
            .replace("https://", "")
}
