package software.doubleshot.demo.deployment

import EnvConfig
import com.fkorotkov.kubernetes.extensions.*
import io.fabric8.kubernetes.api.model.IntOrString
import io.fabric8.kubernetes.client.DefaultKubernetesClient


const val applicationName = "demo"

val applicationLabels = mapOf(
        "app" to applicationName
)
val applicationImage = "registry.gitlab.com/seobrien4/testcorekube/demo-api:${EnvConfig.commitSha}"
val webImage = "registry.gitlab.com/seobrien4/testcorekube/demo-web:${EnvConfig.commitSha}"
val nameSpace = System.getenv("KUBE_NAMESPACE") ?: "demo-localdev"


suspend fun main() {
    println("======================")
    println("Deploying Kotlin K8 DSL Demo API")
    println("NameSpace: $nameSpace")
    println("CI Project Path Slug: ${EnvConfig.ciProjectPathSlug}")
    println("CI Environment Path Slug: ${EnvConfig.ciEnvironmentSlug}")
    val client = DefaultKubernetesClient()
    println("Creating Deployment")
    client.apps().deployments().inNamespace(nameSpace).createOrReplace(DemoAPIDeployment())
    client.apps().deployments().inNamespace(nameSpace).createOrReplace(WebDeployment())
    println("Creating Service")
    client.services().inNamespace(nameSpace).createOrReplace(DemoAPIService())
    client.services().inNamespace(nameSpace).createOrReplace(WebService())
    println("Creating Ingress")
    client.inNamespace(nameSpace).extensions().ingresses().createOrReplace(
            newIngress {
                metadata {
                    name = "$applicationName-ingress"
                    annotations = gitLabAnnotations + ingressAnnotations

                }
                spec {
                    tls = listOf(
                            newIngressTLS {
                                hosts = listOf(
                                        EnvConfig.hostName
                                )
                                secretName = "$nameSpace-$applicationName-tls-secret"
                            }
                    )
                    rules = listOf(
                            newIngressRule {
                                host = EnvConfig.hostName
                                http = newHTTPIngressRuleValue {
                                    paths = listOf(
                                            newHTTPIngressPath {
                                                path = "/api/random"
                                                backend = newIngressBackend {
                                                    serviceName = DemoAPIService.serviceName
                                                    servicePort = IntOrString(DemoAPIService.apiPort)
                                                }
                                            },
                                            newHTTPIngressPath {
                                                path = "/"
                                                backend = newIngressBackend {
                                                    serviceName = WebService.serviceName
                                                    servicePort = IntOrString(WebService.webPort)
                                                }
                                            }
                                    )
                                }
                            }
                    )
                }
            }
    )
}